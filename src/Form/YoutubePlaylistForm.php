<?php

namespace Drupal\youtube_playlist\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the youtube playlist entity edit forms.
 */
class YoutubePlaylistForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $result = parent::save($form, $form_state);

    $entity = $this->getEntity();

    $message_arguments = ['%label' => $entity->toLink()->toString()];
    $logger_arguments = [
      '%label' => $entity->label(),
      'link' => $entity->toLink($this->t('View'))->toString(),
    ];

    switch ($result) {
      case SAVED_NEW:
        $this->messenger()->addStatus($this->t('New youtube playlist %label has been created.', $message_arguments));
        $this->logger('youtube_playlist')->notice('Created new youtube playlist %label', $logger_arguments);
        break;

      case SAVED_UPDATED:
        $this->messenger()->addStatus($this->t('The youtube playlist %label has been updated.', $message_arguments));
        $this->logger('youtube_playlist')->notice('Updated youtube playlist %label.', $logger_arguments);
        break;
    }

    $form_state->setRedirect('entity.youtube_playlist.canonical', ['youtube_playlist' => $entity->id()]);

    return $result;
  }

}
