<?php

namespace Drupal\youtube_playlist;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a youtube playlist entity type.
 */
interface YoutubePlaylistInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

}
